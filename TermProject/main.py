''' @file    main.py
    @brief   Calls task functions for encoder assignment
    @details This code selects which encoder to find rotational location of, 
             and introduces the pin values. Also is the main point where user 
             interactions and encoder operations communicate. 
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import pyb
import task_user
import task_driver
import task_controller
import task_calibration
import task_panel
import task_imu
import shares



# Stores variables for interacting with pins on Nucleo board
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

# Sets which encoder we will be drawing values from

motor1dutycycle = shares.Share(0)
motor2dutycycle = shares.Share(0)

clear_command = shares.Share(False)
run_flag = shares.Share(False)
calib_prompt = shares.Share(0)
cal_values = shares.Share([0,0,0,0,0,0])
calib_flag = 1

# Sets the timing delay used in the tasks
controller_period = 100 # in ms
collection_period = 10  # in ms

print_data_1 = [0,0,0,0]
data_1 = [0,0,0,0]
data_2 = [0,0,0,0]


if __name__ =='__main__':
    

    
    
    # Establishes running the driver and controller
    driver_values = task_driver.Task_Driver()
    controller = task_controller.Task_Controller()
    calibration = task_calibration.Task_Calibration()
    panel = task_panel.Task_Panel()
    angles = task_imu.Task_IMU()

    #variable with user commands initiated
    user = task_user.Task_User(controller_period)
                            
    # Prints the intsructions
    user.instructions()

    
    # Main looping function for whole system
    while True:
        try:
            
            # Interacts with user input
            user.run(print_data_1, run_flag, clear_command, calib_prompt)
            
            # Checks for and runs panel calibration at beginning
            if calib_flag == 1:
                calib_flag = calibration.panel_calib(calib_prompt, cal_values)
            
            # Gathers sensor inputs, runs motors, and calculates controller
            else:
                data_1 = panel.read_positions(collection_period, cal_values)
                data_2 = angles.read_angles(collection_period)
                driver_values.run(motor1dutycycle, motor2dutycycle, clear_command)
                print_data_1 = controller.run(motor1dutycycle, motor2dutycycle, data_1, data_2, run_flag, cal_values, controller_period)

            
        except KeyboardInterrupt:
            print('You have terminated the session')
            break
        
