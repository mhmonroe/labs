''' @file    driver.py
    @brief   File defining the motor driver class for a DRV8847.
    @details Interfaces with task_driver.py to allow other tasks to control
             motors without needing its own logic.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 27, 2021
'''
import pyb
import utime

class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the
                    Motor class which can be used to perform motor
                    control.
                    
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    def __init__ (self, sleep1, sleep2, fault):
        ''' @brief      Initializes and returns a DRV8847 object.
        '''
        self.sleep_pin = pyb.Pin (sleep1, sleep2)
        self.fault = fault
        self.FaultInt = pyb.ExtInt(self.fault, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        pass
    
    def enable (self):
        ''' @brief      Brings the DRV8847 out of sleep mode.
        '''
        self.fault_test = False
        self.sleep_pin.high()
        utime.sleep_us(25)
        self.fault_test = True
        pass
    
    def fault_cb (self, IRQ_src):
        ''' @brief      Callback function to run on fault condition.
            @param      IRQ_src The source of the interrupt request.
        '''
        #check to see if we are ignoring fault command
        if self.fault_test == True:
            self.sleep_pin.low()
            print('fault has occured, press c to clear fault')
        pass
        
    def motor (self, pin1, pin2, ch1, ch2):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @return     An object of class Motor
        '''
        return Motor(pin1, pin2, ch1, ch2)
    
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
    def __init__ (self, pin1, pin2, ch1, ch2):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        '''
        self.tim3 = pyb.Timer(3, freq = 20000)
        self.t3ch1 = self.tim3.channel(ch1, pyb.Timer.PWM, pin=pin1)
        self.t3ch2 = self.tim3.channel(ch2, pyb.Timer.PWM, pin=pin2)
        
        pass
    
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor
        '''
        # if forward, if reverse
        if duty < 0:
            self.t3ch1.pulse_width_percent(abs(duty))
            self.t3ch2.pulse_width_percent(0)
        elif duty == 0:
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(0)
        elif duty > 0:
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(abs(duty))
        pass
    
