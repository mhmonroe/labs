''' @file    closedloop.py
    @brief   Negative feedback loop to reach a desired input.
    @details Uses a proportional and integral controller to reach a desired value
             using gains and measured data
    @author  Marcus Monroe
    @author  Marco Leen
    @date    December 3, 2021
'''

from ulab import numpy as np

class ClosedLoop:
    ''' @brief      Class with the logic of the PI controller used for any
                    desired controlling.
        @details    Our code takes the difference between the measured and reference
                    values, then takes a numeric integral of the values. This is 
                    then mulitiplied to an integral gain and a added to the error 
                    value multiplied by the proportional gain.
    '''
    def __init__ (self, K_p):
        ''' @brief      Constructor for the ClosedLoop class
            @param      K_p
        '''
        self.Kp = K_p
    
    
    def run(self, ref, meas):
        ''' @brief      Takes the error and applies appropiate gains
            @param      ref
            @param      meas
            @param      CL_time
            @return     self.error
        '''
        self.difference = (ref - meas)
        
        self.error = np.dot(self.Kp , self.difference)
        
        return self.error
    
    def get_Kp(self):
        ''' @brief  Returns the proportional gain.
            @return self.Kp
        '''
        return(self.Kp)
    
    def set_Kp(self, Kp_new):
        ''' @brief  Sets a new proportional gain.
            @return Kp_new
        '''
        self.Kp = Kp_new
          

        