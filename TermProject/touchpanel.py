''' @file     touchpanel.py
    @brief    Defines the class for the touch panel object.
    @details  Controls the methods for the touch panel. Controls reading data
              for the position of the ball and for collecting data neccessary
              for calibration.
    @author   Marcus Monroe
    @author   Marco Leen
    @date     December 6, 2021
'''
import pyb
import utime
from ulab import numpy as np

class Panel:
    ''' @brief    Class created to interface with the touch panel.
        @details  Contains the methods used to read values from the touch
                  panels and to calibrate the touch panel in the calibration
                  task.
    '''          
    def __init__(self,xp,xm,yp,ym):
        ''' @brief    Constructor which creates arrays and variables.
            @details  Creates arrays and variables that are needed for 
                      reading position values and for calibration.
            @param    xp
            @param    xm
            @param    yp
            @param    ym
        ''' 
        self.xp_pin = xp
        self.xm_pin = xm
        self.yp_pin = yp
        self.ym_pin = ym
        self.touch_flag = 0
        
        self.Kxx = 0
        print('Kxx' , self.Kxx )
        self.Kxy = 0
        self.Kyy = 0
        self.Kyx = 0
        self.x_0 = 0
        print('Xc',self.x_0)
        self.y_0 = 0
        print('Yc',self.y_0)
        
        # Setting blank matrices for calibration
        self.n = 9
        self.x = np.array(np.zeros((self.n,1)))
        self.y = np.array(np.zeros((self.n,1)))
        self.ADCx = np.array(np.zeros((self.n,1)))
        self.ADCy = np.array(np.zeros((self.n,1)))
        self.ones_column = np.array(np.ones((self.n,1)))
        
    def read_x(self):
        ''' @brief      Reads the x position from the ADC.
            @return     self.ADCx_value
        '''
        self.xp = pyb.Pin(self.xp_pin,pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(self.xm_pin,pyb.Pin.OUT_PP)
        
        self.xp.high()
        self.xm.low()
        
        self.yp = pyb.Pin(self.yp_pin,pyb.Pin.IN)
        self.ym = pyb.ADC(self.ym_pin)
        
        self.ADCx_value = self.ym.read()
        
        return self.ADCx_value
        
    def read_y(self):
        ''' @brief      Reads the y position from the ADC.
            @return     self.ADCy_value
        '''
        self.yp = pyb.Pin(self.yp_pin,pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(self.ym_pin,pyb.Pin.OUT_PP)
        
        self.yp.high()
        self.ym.low()
        
        self.xp = pyb.Pin(self.xp_pin,pyb.Pin.IN)
        self.xm = pyb.ADC(self.xm_pin)
        
        self.ADCy_value = self.xm.read()
        
        return self.ADCy_value
        
    def read_z(self):
        ''' @brief      Reads if a ball is on the platform
            @return     self.touch_flag
        '''
        self.yp = pyb.Pin(self.yp_pin,pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.xp_pin,pyb.Pin.OUT_PP)
        
        self.yp.high()
        self.xp.low()
        
        self.ym = pyb.Pin(self.ym_pin,pyb.Pin.IN)
        self.xm = pyb.ADC(self.xm_pin)
        
        self.z_value = self.xm.read()
        
        if 40<self.z_value<4050:
            self.touch_flag = 1
        else:
            self.touch_flag=0
            
        return self.touch_flag
    
    def set_calibration(self, cal_values):
        ''' @brief      Sets gains for the touch panel based on passed through values.
            @param      cal_values
        '''
        self.Kxx = cal_values[0]
        print('Kxx' , self.Kxx )
        self.Kxy = cal_values[2]
        self.Kyy = cal_values[3]
        self.Kyx = cal_values[1]
        self.x_0 = cal_values[4] +12
        print('Xc',self.x_0)
        self.y_0 = cal_values[5] +10
        print('Yc',self.y_0)
    
    def calibrate(self,meas_num_i, xi, yi):
        ''' @brief      Sets gains for the touch panel based on passed through values.
            @param      meas_num_i
            @param      xi
            @param      yi
            @return     self.ADCx, self.ADCy
        '''
        # Recording entered values
        self.x[(meas_num_i-1)] = xi
        self.y[(meas_num_i-1)] = yi
        
        # Recording ADCx values and averaging
        self.ADCx_1 = self.read_x()
        self.ADCy_1 = self.read_y()
        utime.sleep_us(5)
        self.ADCx_2 = self.read_x()
        self.ADCy_2 = self.read_y()
        utime.sleep_us(5) 
        self.ADCx_3 = self.read_x()
        self.ADCy_3 = self.read_y()
        utime.sleep_us(5)
        self.ADCx_4 = self.read_x()
        self.ADCy_4 = self.read_y()
        utime.sleep_us(5) 
        self.x[(meas_num_i-1)] = xi
        self.y[(meas_num_i-1)] = yi
        self.ADCx_5 = self.read_x()
        self.ADCy_5 = self.read_y()
        utime.sleep_us(5)
        self.ADCx_6 = self.read_x()
        self.ADCy_6 = self.read_y()
        utime.sleep_us(5) 
        self.ADCx_7 = self.read_x()
        self.ADCy_7 = self.read_y()
        utime.sleep_us(5)
        self.ADCx_8 = self.read_x()
        self.ADCy_8 = self.read_y()
        
        # Recording ADCx as an average 
        self.ADCx = (self.ADCx_1+self.ADCx_2+self.ADCx_3+self.ADCx_4+self.ADCx_5+self.ADCx_6+self.ADCx_7+self.ADCx_8)/8
        
        # Recording ADCy as an average
        self.ADCy = (self.ADCy_1+self.ADCy_2+self.ADCy_3+self.ADCy_4+self.ADCy_5+self.ADCy_6+self.ADCy_7+self.ADCy_8)/8
            
        return( self.ADCx, self.ADCy)
        
    def get_values(self, cal_values):
        ''' @brief      Sets gains for the touch panel based on passed through values.
            @param      cal_values
            @return     self.locations
        '''
        self.Kxx = cal_values[0]
        self.Kxy = cal_values[2]
        self.Kyy = cal_values[3]
        self.Kyx = cal_values[1]
        self.x_0 = cal_values[4]
        self.y_0 = cal_values[5]
        
        self.x_value = self.Kxx*self.read_x() + self.Kxy*self.read_y() + self.x_0
        self.y_value = self.Kyx*self.read_x() + self.Kyy*self.read_y() + self.y_0
        self.z_value = self.read_z()
        
        self.locations = np.array([self.x_value, self.y_value, self.z_value])
        
        return self.locations
            
