''' @file    task_controller.py
    @brief   Task used for controlling motors.
    @details The task gets passed information that is then used with driver.py
             to control the speed of the motors.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''
import utime
import closedloop
from ulab import numpy as np


class Task_Controller:
    ''' @brief    Class created to interface with the motor driver.
        @details  Contains the methods used to allow other tasks to control
                  motors without needing its own motor logic.
    '''     
    def __init__(self):
        ''' @brief    Constructor that creates necessary variables and arrays.
            @details  Instantiates the touch panel object and the IMU object.
                      Creates the arrays needed for calculating the output
                      duty cycle using the gains for the torque and the
                      vallues needed for the alpha beta filtering.
            @return   print_values
        ''' 
        
        
        #K matrix from MatLab calculations
        K =[ (-6656/225), (122788/2025), (-90224/2025), (108179/2025)]
        
        #K matrices with adjustments
        Kx = np.array([ (-0.8*K[0]) , (-1*K[1]) , (1.75*K[2]) , (-0.0075*K[3]) ])
        Ky = np.array([ (0.8*K[0]) , (-1*K[1]) , (.75*K[2]) , (-0.0075*K[3]) ])
        
        # Instantiating closed loop variables
        self.torque_1 = closedloop.ClosedLoop(Kx)
        self.torque_2 = closedloop.ClosedLoop(Ky)
        
        #initial state vectors
        self.state_x_prev = np.array(np.zeros((4,1)))
        self.state_y_prev = np.array(np.zeros((4,1)))
        self.state_x_new  = np.array(np.zeros((4,1)))
        self.state_y_new  = np.array(np.zeros((4,1)))
        
        # Establishing where we want our refernece to be
        self.ref_state_x  = np.array(np.zeros((4,1)))
        self.ref_state_y  = np.array(np.zeros((4,1)))
        
        # Establishing array to pass printed values through a share
        self.print_data = [0,0,0,0]
        self.positions = [0,0,0,0]
        self.angles = [0,0,0,0]
        
        # Establishing Duty cycle variables
        self.dutycycle = [0,0]
        self.dc        = [0,0]

        # Creating time counter for task frequency
        self.next_time = 0
        self.start_time= utime.ticks_ms()
        
    def run(self, motor1dutycycle, motor2dutycycle, data_1, data_2, run_flag, cal_values, collection_period):
        ''' @brief    Calculates the duty cycle needed to balance the ball.
            @details  Reads the position of the ball on the plate if present
                      and the orientation of the IMU. Calculates the velocity
                      of the ball and the angular velocity of the plate using
                      alpha beta filtering. Calculates the desired duty cycle
                      of the motors using the specified gains and the
                      calculated values in the state vectors. 
            @param    motor1dutycycle
            @param    motor2dutycycle
            @param    data_1
            @param    data_2
            @param    run_flag
            @param    cal_values
            @param    collection_period
            @return   print_data
        ''' 
        
        # Collecting values from other tasks
        self.positions = data_1  # mm to meters
        self.angles = data_2      # degrees to radians
        
        # Setting time for task frequency
        self.collection_period = collection_period
        self.current_time = utime.ticks_diff(utime.ticks_ms(), self.start_time)
        
        if self.current_time > self.next_time :
            self.next_time = self.current_time + collection_period
            
            
            # Creating state arrays
            self.state_x_new[0,0] =  -self.positions[0] #x
            self.state_x_new[1,0] =  self.angles[0]    #theta
            self.state_x_new[2,0] =  self.positions[2] #x_dot
            self.state_x_new[3,0] =  self.angles[2]   #theta_dot
            
            self.state_y_new[0,0] = self.positions[1]  #x
            self.state_y_new[1,0] = self.angles[1]    #theta
            self.state_y_new[2,0] = self.positions[3] #x_dot
            self.state_y_new[3,0] = self.angles[3] #theta_dot
            
            #Recording values to use in next delta calculation
            self.state_x_prev = self.state_x_new
            self.state_y_prev = self.state_y_new
            
            # Sharing data that can be used later
            self.print_data[0] = self.state_x_new[0,0]
            self.print_data[1] = self.state_x_new[1,0]
            self.print_data[2] = self.state_y_new[0,0]
            self.print_data[3] = self.state_y_new[1,0]
            
           
            # Finding Motor Torque with a closed loop object  
            self.Tx_1_array = self.torque_1.run(self.ref_state_x,self.state_x_new)
            self.Tx_2_array = self.torque_2.run(self.ref_state_y,self.state_y_new)
            
            self.Tx_1 = self.Tx_1_array[0] # This is for program syntax
            self.Tx_2 = self.Tx_2_array[0] # This is for program syntax
    
            # Finding Motor Duty Cycle 
            self.R   = 2.21     # Ohms
            self.Kt  = .138   # Nm/A
            self.Vdc = 12       # Volts
            self.calm_down = 1 # to help the motor relax

            
            if run_flag.read() == True:
                
                # Converts torque to duty cylce
                self.dutycycle[0] = self.calm_down*self.Tx_1*(100*self.R)/(4*self.Kt*self.Vdc) 
                self.dutycycle[1] = self.calm_down*self.Tx_2*(100*self.R)/(4*self.Kt*self.Vdc) 
                
                
   # Running a low pass filter on the dutycyle by creating a max allowable torque
                
                self.max = 65 # we find this to correspond to maximum productive torque
                
                # Motor 1 filter
                if self.dutycycle[0] > (self.max):
                    self.dc[0] = self.max
                elif self.dutycycle[0] < -(self.max):
                    self.dc[0] = -self.max
                elif self.dutycycle[0] < 0:
                    self.dc[0] = self.dutycycle[0] 
                else:
                    self.dc[0] = self.dutycycle[0] 
                
                # Motor 2 filter
                if self.dutycycle[1] > (self.max):
                    self.dutycycle[1] = (self.max)
                elif self.dutycycle[1] < -self.max:
                    self.dc[1] = -self.max 
                elif self.dutycycle[1] < 0:
                    self.dc[1] = self.dutycycle[1]
                else: 
                    self.dc[1] = self.dutycycle[1]
                 
                # We find the board to get stuck, so this prevents the stuch condition
                if self.angles[1] > 0.2:
                    self.dc[1] = abs(self.dutycycle[1])
                    
                
                # Write duty cyle to shared variable for task_driver 
                motor1dutycycle.write(self.dc[0])
                motor2dutycycle.write(self.dc[1])
                

                
        return self.print_data
