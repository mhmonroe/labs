''' @file    task_imu.py
    @brief   Task used for filtering imu data.
    @details The task gets passed information that is then filtered with
             an alpha-beta filter and reports location and velocity values.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    December 7, 2021
'''
import utime
import imu
from pyb import I2C

class Task_IMU:
    ''' @brief    Class created to interface with the imu values.
        @details  Contains the alpha beta filtering for received values.
    '''     
    def __init__(self):
        ''' @brief    Constructor that creates necessary variables and arrays.
            @details  Instantiates tthe IMU object. Creates the arrays needed 
                      for calculating the output duty cycle using the values 
                      needed for the alpha beta filtering.
        '''              
        
        # Instantiating objects
        i2c = I2C(1,I2C.MASTER)
        self.eul_angles = imu.BNO055(i2c)
        self.eul_angles.change_mode(12)
        
        
        # Alpha and Beta variables
        self.alpha     = 0.85
        self.beta      = 0.005
        self.x         = [0,0] 
        self.rhat      = [0,0]
        self.vhat      = [0,0]
        self.xhat      = [0,0]
        self.next_time = 0
        self.start_time= utime.ticks_ms()
        self.data_2 =  [0,0,0,0]
        
        
    def read_angles(self, collection_period):
        ''' @brief      Collects and filters IMU angle readings.
            @param      collection_period
            @return     data_2
        '''
        
        self.current_time = utime.ticks_diff(utime.ticks_ms(), self.start_time)
        
        if self.current_time > self.next_time :
            self.next_time = self.current_time + collection_period
            
            # Get values from IMU
            self.positions = self.eul_angles.read_angles()
            # Alpha Beta Filtering
            # Filtering linear velocity
            self.filter_time = collection_period/1e3
            
            # Filtering position values
            self.x[0] = -self.positions[0]* (3.14259/180)
            self.x[1] = self.positions[1]* (3.14259/180)
            
            # EQ1
            self.xhat[0] = self.xhat[0] + self.filter_time*self.vhat[0]
            self.xhat[1] = self.xhat[1] + self.filter_time*self.vhat[1]
            # EQ3  (EQ2 is vhat_k = vhat_k1 which is redundant in our code)
            self.rhat[0] = self.x[0]-self.xhat[0]
            self.rhat[1] = self.x[1]-self.xhat[1]
            
            # EQ 4
            self.xhat[0] = self.xhat[0] + self.alpha*self.rhat[0]
            self.xhat[1] = self.xhat[1] + self.alpha*self.rhat[1]
            # EQ 5
            self.vhat[0] = self.vhat[0] + (self.beta*self.rhat[0])/self.filter_time
            self.vhat[1] = self.vhat[1] + (self.beta*self.rhat[1])/self.filter_time
            
            self.data_2 = [ self.xhat[0], self.xhat[1], self.vhat[0] , self.vhat[1] ]
            
            return self.data_2
        else:
           return self.data_2
