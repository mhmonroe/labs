''' @file    task_calibration.py
    @brief   Task used for calibrating the touch panel.
    @details The task either reads from an existing calibration file if one 
             has already been made on the board, or will write its own 
             coefficients and offsets by calculation and user input.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    December 7, 2021
'''
import os
from ulab import numpy as np
import touchpanel

class Task_Calibration:
    ''' @brief   Class for running calibration methods.
        @details Methods for reading from preexisting calibration data or for
                 calculating when data is needed.
    '''
    def __init__(self):
        ''' @brief    Constructor which creates arrays and variables.
            @details  Creates arrays and variables that are needed for 
                      calculating new coefficients and offsets.
        ''' 
        self.touch_pan = touchpanel.Panel('PA7','PA1','PA6','PA0')
        self.state = 1

        self.n = 7
        self.x = np.array(np.zeros((self.n,1)))
        self.y = np.array(np.zeros((self.n,1)))
        self.ADCx = np.array(np.zeros((self.n,1)))
        self.ADCy = np.array(np.zeros((self.n,1)))
        self.ones_column = np.array(np.ones((self.n,1)))
        
        self.Kxx = 0
        self.Kxy = 0
        self.Kyx = 0
        self.Kyy = 0
        self.Xc  = 0
        self.Yc  = 0
        self.x = np.array(np.zeros((self.n,1)))
        self.y = np.array(np.zeros((self.n,1)))
        
    def run_panel_calib(self, calib_prompt):
        ''' @brief    State machine that calculates new coefficients/offsets.
            @details  State machine prompts user to touch panel at specific
                      locations, with the user pressing enter for the next
                      data point to be collected. After each measured value
                      has been recorded coefficients and offsets are 
                      calculated.
            @param    calib_prompt
            @return   self.calib_flag
        ''' 
        if self.state ==1:
            
            print(' Apply pressure to (0,0)[mm], and press enter when ready for data collection. Mantain pressure until next prompt')
           
            self.input = input()
            self.x[0,0] = 0
            self.y[0,0] = 0
            (self.ADCx[0,0], self.ADCy[0,0]) = self.touch_pan.calibrate(1, 0, 0)
            self.state += 1
                
                
        if self.state ==2:
        
            print(' Apply pressure to (40,0)[mm], and press enter when ready for data collection. Mantain pressure until next prompt')
                 
            self.input = input()
            self.x[(self.state-1),0] = 40
            self.y[(self.state-1),0] = 0
            (self.ADCx[(self.state-1),0], self.ADCy[(self.state-1),0]) = self.touch_pan.calibrate(self.state, 40, 0)
            self.state += 1       
         
        if self.state ==3:
        
            print(' Apply pressure to (80,0)[mm], and press enter when ready for data collection. Mantain pressure until next prompt')
                 
            self.input = input()
            self.x[(self.state-1),0] = 80
            self.y[(self.state-1),0] = 0
            (self.ADCx[(self.state-1),0], self.ADCy[(self.state-1),0]) = self.touch_pan.calibrate(self.state, 80, 0)
            self.state += 1 
 
        if self.state ==4:
        
            print(' Apply pressure to (80,40)[mm], and press enter when ready for data collection. Mantain pressure until next prompt')
                    
            self.input = input()
            self.x[(self.state-1),0] = 80
            self.y[(self.state-1),0] = 40
            (self.ADCx[(self.state-1),0], self.ADCy[(self.state-1),0]) = self.touch_pan.calibrate(self.state, 80, 40)
            self.state += 1 
                
        if self.state ==5:
        
            print(' Apply pressure to (-80,-40)[mm], and press enter when ready for data collection. Mantain pressure until next prompt')
                   
            self.input = input()
            self.x[(self.state-1),0] = -80
            self.y[(self.state-1),0] = -40
            (self.ADCx[(self.state-1),0], self.ADCy[(self.state-1),0]) = self.touch_pan.calibrate(self.state, -80, -40)
            self.state += 1
                
        if self.state ==6:
        
            print(' Apply pressure to (0,40)[mm], and press enter when ready for data collection. Mantain pressure until next prompt')
                    
            self.input = input()
            self.x[(self.state-1),0] = 0
            self.y[(self.state-1),0] = 40
            (self.ADCx[(self.state-1),0], self.ADCy[(self.state-1),0]) = self.touch_pan.calibrate(self.state, 0, 40)
            self.state += 1
                
        if self.state ==7:
        
            print(' Apply pressure to (-40,-40)[mm], and enter key when ready for data collection. Mantain pressure until next prompt')
    
            self.input = input()
            self.x[(self.state-1),0] = -40
            self.y[(self.state-1),0] = -40
            (self.ADCx[(self.state-1),0], self.ADCy[(self.state-1),0]) = self.touch_pan.calibrate(self.state, -40, -40)
            self.state += 1
                
        if self.state ==8: 

                    # meas_num_i is which reading we are taking in the array
                    # xi and yi are the real locations
                    
                    # this solves the following matrix math for n data points
                    
                    # xi and yi are the real locations
                    #               [ x1 , y1
                    #                 x2 , y2
                    #                 .    .
                    #                 .    .
                    #                 .    .
                    #                 xn , yn ]
                    # This is denoted as matrix X_prime
                
                    # ADC matrix  = [ ADCx1,ADCy1,1 
                    #                 ADCx2,ADCy2,1 
                    #                    .   .    .
                    #                    .   .    . 
                    #                    .   .    .
                    #                 ADCxn,ADCyn,1 ] 
                    # This is denoted as matrix X
                
                    # Matrix we are solving for:
                    # Calibration coeffs matrix :
                    #               [ Kxx , Kyx
                    #                 Kxy , Kyy
                    #                 x0  , y0 ]
                    # This is denoted as matrix B
            self.X_prime = np.concatenate((self.x,self.y), axis=1)
            self.X = np.array(np.concatenate((self.ADCx, self.ADCy, self.ones_column), axis=1))
            print('X array is:' , self.X)
            self.Xtranspose = self.X.transpose()

            
            
            self.XtdotX = np.dot(self.Xtranspose,self.X)
            self.inv_XtdotX = np.linalg.inv(self.XtdotX)
            self.inv_XtdotX_dot_Xt = np.dot(self.inv_XtdotX,self.Xtranspose)
            
            self.B = np.dot(self.inv_XtdotX_dot_Xt,self.X_prime)
            print(self.B)
            self.Kxx = self.B[0,0]
            self.Kxy = self.B[1,0]
            self.Kyx = self.B[0,1]
            self.Kyy = self.B[1,1]
            self.Xc  = self.B[2,0]
            self.Yc  = self.B[2,1]
            self.calib_flag = 0

        return self.calib_flag             
        
    def panel_calib(self, calib_prompt, cal_values):
        ''' @brief    Reads or writes new coefficients/offsets.
            @details  Determines whether or not a file with coefficients and
                      offsets exists. Creates and writes to a file if one is
                      not present. Calls the run_panel_calib() method if a
                      to calculate the necessary values.
            @param    calib_prompt
            @param    cal_values
            @return   self.calib_flag
        ''' 
        filename = "RT_cal_coeffs.txt"
        if filename  in os.listdir():

            #read from file
            with open(filename, 'r') as f:
                # read the first line of the file
                cal_data_string = f.readline()
                # split the line into multiple strings
                # and then convert each one into a float
                calib_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                cal_values.write(calib_values)
                
                self.touch_pan.set_calibration(cal_values.read())
                self.calib_flag = 0
                
                print('Recieved Calibration Values')
        else:
            print('Panel needs calibration')            
            with open(filename, 'w') as f:

                #Perform manaual calibration
                self.run_panel_calib(calib_prompt)
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = (self.Kxx, self.Kxy, self.Kyx, self.Kyy, self.Xc, self.Yc)
                self.calib_flag = 0
                # Then write the coeffecients to a string
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                
        return self.calib_flag  