''' @file imu.py
    @brief   Reports Euler angle position of sensor
    @details Calibrates the IMU, controls the calibration mode, reports and 
             writes the calibration coeffecients, and prints the sensor 
             angular position and velocity in Euler angles.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    November 11, 2021
'''
from pyb import I2C
import utime
from ulab import numpy as np

class BNO055:
    ''' @brief  Conducts interaction with IMU sensor
        @details Calibrates the IMU, controls the calibration mode, reports and 
                 writes the calibration coeffecients, and prints the sensor 
                 angular position and velocity in Euler angles.     
    '''
    def __init__ (self, i2c):
        ''' @brief      Initializes and returns a BNO055 object.
            @param      i2c
        '''
        # Sets interaction variables
        self.i2c = i2c
        self.buf = bytearray(6)
        
        
    def change_mode (self, mode):
        ''' @brief   Changes mode of IMU sensor
            @param   mode    New mode for calibration
        '''
        self.mode = mode
        
        self.i2c.mem_write(0b0000,0x28,0x3D)
        self.i2c.mem_write(self.mode,0x28,0x3D)
        
    def retrieve_calibration (self):
        ''' @brief   Prints calibration status, ready when all equal 3 
        '''
        self.cal_bytes = self.i2c.mem_read(1,0x28,0x35)
        print("Binary:", '{:#010b}'.format(self.cal_bytes[0]))
        self.cal_status = ( self.cal_bytes[0] & 0b11,
                           (self.cal_bytes[0] & 0b11 << 2) >> 2,
                           (self.cal_bytes[0] & 0b11 << 4) >> 4,
                           (self.cal_bytes[0] & 0b11 << 6) >> 6)
        
        print("Values:", self.cal_status)
        print('\n')
        
        
    def retrieve_coefficients (self):
        ''' @brief   Prints calibration coefficients
        '''
        retrieved_coeff = self.i2c.mem_read(18,0x28,0x55)
        
        acc_x = retrieved_coeff[1] << 8 | retrieved_coeff[0]
        acc_y = retrieved_coeff[3] << 8 | retrieved_coeff[2]
        acc_z = retrieved_coeff[5] << 8 | retrieved_coeff[4]
        
        mag_x = retrieved_coeff[7] << 8 | retrieved_coeff[6]
        mag_y = retrieved_coeff[9] << 8 | retrieved_coeff[8]
        mag_z = retrieved_coeff[11] << 8 | retrieved_coeff[10]
        
        gyr_x = retrieved_coeff[13] << 8 | retrieved_coeff[12]
        gyr_y = retrieved_coeff[15] << 8 | retrieved_coeff[14]
        gyr_z = retrieved_coeff[17] << 8 | retrieved_coeff[16]
        
        
        print('Accelerometer Offsets:','X:', acc_x, 'Y:', acc_y,'Z:', acc_z)
        print('Magnetometer Offsets:','X:', mag_x, 'Y:', mag_y,'Z:', mag_z)
        print('Gyroscope Offsets:','X:', gyr_x, 'Y:', gyr_y,'Z:', gyr_z)
        
    def write_coefficients (self, coeffecients):
        ''' @brief   Writes new calibration coefficients
            @param   coefficients   New calibration coefficients
        '''
        # Setting to calibration mode
        self.i2c.mem_write(0b0000,0x28,0x3D)
        # Writing new coefficients
        self.i2c.mem_write(coeffecients,0x28,0x55)
        # Returning back to mode previously selected
        self.i2c.mem_write(self.mode,0x28,0x3D)
        
    def read_angles (self):
        ''' @brief   Prints angular position as an Euler angle in deg
            @return  self.angles
        '''
        
        euler = self.i2c.mem_read(self.buf,0x28,0x1A)
        
        #Interperating Data
        self.heading = euler[1] << 8 | euler[0]
        self.pitch = euler[5] << 8 | euler[4]
        self.roll = euler[3] << 8 | euler[2]
        
        # Incorperating the sign
        if self.heading > 32767:
            self.heading -= 65536
        if self.roll > 32767:
            self.roll -= 65536
        if self.pitch > 32767:
            self.pitch -= 65536
        
        # converts to degrees
        self.pitch /= 16
        self.roll /= 16
        self.heading /= 16
        
        #create array of important values
        self.angles = np.array([int(self.pitch), int(self.roll)])
  
        # attached comment allows position to be printed to same line
        return self.angles
          
        
    def read_velocity (self):
        ''' @brief   Prints angular velocity in Euler angles in deg/s
            @return  self.roll_velocity
            @return  self.pitch_velocity
        '''
        
        euler = self.i2c.mem_read(self.buf,0x28,0x1A)
        
        #Interperating Data
        self.heading = euler[1] << 8 | euler[0]
        self.pitch = euler[5] << 8 | euler[4]
        self.roll = euler[3] << 8 | euler[2]
        
        # Incorperating the sign
        if self.heading > 32767:
            self.heading -= 65536
        if self.roll > 32767:
            self.roll -= 65536
        if self.pitch > 32767:
            self.pitch -= 65536
        
        # converts to degrees 
        self.pitch /= 16
        self.roll /= 16
        self.heading /= 16
        
        # Saves these to be compared to future position
        self.pitch_previous = self.pitch
        self.roll_previous = self.roll
        self.heading_previous = self.heading
        self.previous_time = self.current_time
        
        # Now re-doing the position recording process
        euler = self.i2c.mem_read(self.buf,0x28,0x1A)
        
        #Interperating Data
        self.heading = euler[1] << 8 | euler[0]
        self.pitch = euler[5] << 8 | euler[4]
        self.roll = euler[3] << 8 | euler[2]
        
        # Incorperating the sign
        if self.heading > 32767:
            self.heading -= 65536
        if self.roll > 32767:
            self.roll -= 65536
        if self.pitch > 32767:
            self.pitch -= 65536
        
        # Converting to degrees
        self.pitch /= 16
        self.roll /= 16
        self.heading /= 16
        
        # Calculates velocity with first and second recorded value
        # using the time called in main when testing
        self.pitch_velocity = (self.pitch-self.pitch_previous)/0.0005
        self.roll_velocity = (self.roll-self.roll_previous)/0.0005
        self.heading_velocity = (self.heading-self.heading_previous)/0.0005 
        
  
        # attached comment allows position to be printed to same line
        return self.roll_velocity
        return self.pitch_velocity
   
        

        