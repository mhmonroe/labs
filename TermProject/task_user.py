''' @file    task_user.py
    @brief   Interperets user data and reports appriate values  
    @details The user input into the keyboard will determine which values are
             saved and which values are printed. 
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''
import utime
import pyb
import array
class Task_User:
    ''' @brief    Class created to interface with the motor driver.
        @details  Contains the methods used to allow other tasks to control
                  motors without needing its own motor logic.
    '''       
    def __init__(self, collection_period):
        ''' @brief Sets variables for the file and calls the hardware    
            @param collection_period
        ''' 
        self.serport = pyb.USB_VCP()

        self.collection_period = collection_period

        self.collect_next_time = 0
        self.collect_data_1 = False


        
        #setting array sizes and counters
        self.array_num = 30000/int(self.collection_period)
        self.data_time_array = array.array( 'f', [self.array_num]*0)
        self.data_x_position_array = array.array( 'f', [self.array_num]*0)
        self.data_y_theta_array = array.array( 'f', [self.array_num]*0)
        self.data_y_position_array = array.array( 'f', [self.array_num]*0)
        self.data_x_theta_array = array.array( 'f', [self.array_num]*0)
      
        self.print_data = False
        self.i = 0
        self.k = 0
        self.startTime = utime.ticks_ms()    
              
    def run(self, print_data_1, run_flag, clear_command, calib_prompt):
        ''' @brief   Main method that controls user interface.
            @details Prints the user interface, controls all logic for commands
                     that the user can input.
            @param   print_data_1
            @param   run_flag
            @param   clear_command
            @param   calib_prompt
        ''' 

        self.currentTime = utime.ticks_diff(utime.ticks_ms(), self.startTime)
        self.data_1 = print_data_1

         
        if self.serport.any():
            command = self.serport.read(1)
            
            if command == b's' or command == b'S':
                self.collect_data_1 = False
                self.collect_data_2 = False
                print(' Data collection was stopped ')
                self.print_data = True
                
            elif command == b'b' or command == b'B':
                run_flag.write(True)
                
            elif command == b'x' or command == b'X':
                run_flag.write(False) 
                
            elif command == 'r' :
                calib_prompt.write(1) 

            elif command == b'c' or command == b'C':
                 clear_command.write(True)
                 print('cleared fault')

            if command == b'g':
            
                self.collect_next_time = self.collection_period
                self.collect_data_1 = True
                print('collecting positional data...')
                self.startTime = utime.ticks_ms()
                self.currentTime = 0
                # clearing array counter
                self.k = 0
                # zeroing the arrays
                self.data_time_array = array.array('f', [0]*int(self.array_num))
                self.data_x_position_array = array.array('f', [0]*int(self.array_num))
                self.data_y_theta_array = array.array('f', [0]*int(self.array_num))
                self.data_y_position_array = array.array('f', [0]*int(self.array_num))
                self.data_x_theta_array = array.array('f', [0]*int(self.array_num))
                        
                         
        if self.collect_data_1 and self.currentTime >= self.collect_next_time:
            
            self.array_time = float(self.currentTime)/100
            self.data_time_array[self.k] = round(self.array_time,1)/10
            self.data_x_position_array[self.k] = (self.data_1[0])
            self.data_y_theta_array[self.k] = (self.data_1[1])
            self.data_y_position_array[self.k] = (self.data_1[2])
            self.data_x_theta_array[self.k] = (self.data_1[3])
            self.collect_next_time += self.collection_period
            
            #counter to determine array list lengths
            self.k +=1
            if self.currentTime >= 30000:
                self.print_data = True
                self.collect_data_1 = False
             
        if self.print_data ==True and self.i < self.array_num :
            if self.data_time_array[self.i] > 0:
                print(self.data_time_array[self.i],'s', self.data_x_position_array[self.i],'m', self.data_y_theta_array[self.i],'rad', self.data_y_position_array[self.i],'m', self.data_x_theta_array[self.i],'rad')
                self.i += 1
            else:
                pass
        else:
            self.print_data = False
            self.i = 0

        
    def instructions(self):
        ''' @brief Method to print out the formatted instructions  
        ''' 
        print('+-----------------------------------------+')
        print('| b : Start running motors for balancing  |')
        print('| c : Clear fault condition on driver     |')
        print('| g : Collect x,y,theta_x and theta_y     |')
        print('|     data for 30 secs and print it to    |')
        print('|     PuTTY as a list                     |')
        print('| s : End data collection prematurely     |')
        print('+-----------------------------------------+')  
        
        
   
        
    