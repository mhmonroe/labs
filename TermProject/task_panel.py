''' @file    task_panel.py
    @brief   Task used for filtering touch panel data.
    @details The task gets passed information that is then filtered with
             an alpha-beta filter and reports location and velocity values.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    December 7, 2021
'''
import utime
import touchpanel
from ulab import numpy as np

class Task_Panel:
    ''' @brief    Class created to interface with the touch panel values.
        @details  Contains the alpha beta filtering to receive values.
    '''     
    def __init__(self):
        ''' @brief    Constructor that creates necessary variables and arrays.
            @details  Instantiates the touch panel object.
                      Creates the arrays needed for calculating the output
                      duty cycle using the gains for the torque and the
                      vallues needed for the alpha beta filtering.
        '''              
        
        # Instantiating objects
        self.location = touchpanel.Panel('PA7','PA1','PA6','PA0')
        
        
        # Alpha and Beta variables
        self.alpha     = 0.85
        self.beta      = 0.005
        self.x         = [0,0] 
        self.rhat      = [0,0]
        self.vhat      = [0,0]
        self.xhat      = [0,0]
        self.next_time = 0
        self.start_time= utime.ticks_ms()
        self.data_1 =  [0,0,0,0]
        
    def read_positions(self, collection_period, cal_values):
        ''' @brief      Gets and filters touch panel values based on passed through values.
            @param      collection_period
            @param      cal_values
            @return     data_1
        '''
        
        self.current_time = utime.ticks_diff(utime.ticks_ms(), self.start_time)
        
        if self.current_time > self.next_time :
            self.next_time = self.current_time + collection_period
            
            # Read values from panel
            self.positions = self.location.get_values(cal_values.read())
            
            # Makes sure we only read real values when panel is touched
            if self.positions[2] == 0:
                self.positions = np.array([0,0,0])
            
            # Alpha Beta Filtering
            # Filtering linear velocity
            self.filter_time = collection_period/1e3
            
            # Filtering position values
            self.x[0] = -self.positions[0]* (1/1000)
            self.x[1] = self.positions[1]* (1/1000)
            
            # EQ1
            self.xhat[0] = self.xhat[0] + self.filter_time*self.vhat[0]
            self.xhat[1] = self.xhat[1] + self.filter_time*self.vhat[1]
            # EQ3  (EQ2 is vhat_k = vhat_k1 which is redundant in our code)
            self.rhat[0] = self.x[0]-self.xhat[0]
            self.rhat[1] = self.x[1]-self.xhat[1]
            
            # EQ 4
            self.xhat[0] = self.xhat[0] + self.alpha*self.rhat[0]
            self.xhat[1] = self.xhat[1] + self.alpha*self.rhat[1]
            # EQ 5
            self.vhat[0] = self.vhat[0] + (self.beta*self.rhat[0])/self.filter_time
            self.vhat[1] = self.vhat[1] + (self.beta*self.rhat[1])/self.filter_time
            
            self.data_1 = [self.xhat[0], self.xhat[1], self.vhat[0] , self.vhat[1]]
            
            return self.data_1
        
        else:
           return self.data_1 
