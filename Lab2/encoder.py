''' @file encoder.py
    @breif   Gets encoder values from hardware
    @details Interacts with the hardware to report the encoder value
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import pyb
class Encoder:
    
    def __init__(self, timerN, period, pin1, pin2, commandtime):
        ''' @breif Sets variables for the file and calls the hardware 
            @param timerN    
            @param period 
            @param pin1
            @param pin2
        ''' 
        self.encoderposition = 0
        self.delta = 0
        self.prevtick = 0
        self.newtick = 0
        self.period = period
        self.timerN = timerN
        self.commandtime = commandtime
    
        self.timer = pyb.Timer (self.timerN , prescaler = 0 , period = self.period)
        self.t4ch1 = self.timer.channel(1 , pyb.Timer.ENC_A , pin = pin1)
        self.t4ch2 = self.timer.channel(2 , pyb.Timer.ENC_B , pin = pin2)
        
    
    def update(self):
        ''' @breif     Reports encoder postition
            @return Encoder position
        ''' 
        self.prevtick = self.newtick
        self.newtick = self.timer.counter()
        self.delta = self.newtick - self.prevtick
                
        if self.delta >= self.period/2:
            self.delta -= self.period
        elif self.delta <= -self.period/2:
            self.delta += self.period
                    
        self.encoderposition += self.delta
        
        return self.encoderposition
    
    def get_position(self):
        ''' @breif     Reports encoder position
            @return Encoder position
        ''' 
        return self.encoderposition
    
    def set_position(self,position): 
        ''' @breif     Zeros the encoder value
            @param position
        ''' 
        self.encoderposition = position
        
        
    def get_delta(self):
        ''' @breif     Reports encoder rate of change
            @return Encoder rate of change
        ''' 
        self.prevtick = self.newtick
        self.newtick = self.timer.counter()
        self.delta = self.newtick - self.prevtick
        
        if self.delta >= self.period/2:
            self.delta -= self.period
        elif self.delta <= -self.period/2:
            self.delta += self.period
            
        # ticks to radians
        self.radians = self.delta/4000 *2*3.14159
        # radians to rad/s
        self.rad_sec = self.radians/(self.commandtime/(1e6))
            
        return self.rad_sec