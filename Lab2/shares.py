''' @file shares.py
    @breif   G
    @details I
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 27, 2021
'''

class Share:
    
    
    def __init__(self, start=None):
        
        self.buffer = start
        
    def write(self,item):
        self.buffer = item
        
    def read(self):
        return self.buffer
    
    
class Queue:
    
    
    
    def __init__(self):
        
        
        self.buffer = []
        
    def put(self, item):
        
        
        self.buffer.append(item)
        
    def get(self):
        
        
        return self.buffer.pop(0)
    def num_in(self):
        
        
        return len(self.buffer)