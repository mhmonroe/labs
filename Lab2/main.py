''' @file main.py
    @breif   Calls task functions for encoder assignment
    @details This code selects which encoder to find rotational location of, 
    and introduces the pin values. Also is the main point where user 
    interactions and encoder operations communicate. 
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import pyb
import task_encoder
import task_user
import task_driver
import shares


# Stores variables for interacting with pins on Nucleo board
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

# Sets which encoder we will be drawing values from

motor1speed = shares.Share(0)
motor2speed = shares.Share(0)
zero_flag = shares.Share(0)
clear_command = shares.Share(False)

# Sets the timing delay used in the tasks
command_period = 10
collection_period = 1000




if __name__ =='__main__':
    
   #variable with postion and delta intitated
    encoder_value_1 = task_encoder.Task_Encoder(command_period, 4, pinB6, pinB7, 1)
            
                        #variable with postion and delta intitated
    encoder_value_2 = task_encoder.Task_Encoder(command_period, 8, pinC6, pinC7, 2)
    
    driver_values = task_driver.Task_Driver()

            #variable with user commands initiated
    user = task_user.Task_User(command_period,collection_period)
                            
            # Prints the intsructions
    user.instructions()
        
    # Gets values for the encoder to be printed   
    while True:
        try:
    # Gets values for the encoder to be printed
            # Actual interaction with the Tasks
            driver_values.run(motor1speed, motor2speed, clear_command)
            data_1 = encoder_value_1.run(zero_flag)
            data_2 = encoder_value_2.run(zero_flag)
            
            user.run(data_1,data_2, motor1speed, motor2speed, zero_flag, clear_command)
            
            
        except KeyboardInterrupt:
            print('You have terminated the session')
            break
        
