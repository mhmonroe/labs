''' @file task_encoder.py
    @breif   Gets encoder values from encoder.py 
    @details Gets encoder values, passes appropiate values to encoder.py, and 
    specifically controls the zeroing of the encoder position
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import encoder
import utime


class Task_Encoder:
    
    def __init__(self, period, timerN, pin1, pin2, flag_num):
        ''' @breif Sets variables for the file and calls the hardware 
            @param timerN    
            @param period 
            @param pin1
            @param pin2
        ''' 
        self.runs = 0
        self.commandtime = period
        self.pin1 = pin1
        self.pin2 = pin2
        self.flag_num = flag_num
        
        self.enc = encoder.Encoder(timerN,65535,self.pin1,self.pin2, self.commandtime)
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.commandtime)
        
              
    def run(self, zero_flag):
        ''' @breif Zeros the posititon of the encoder  
            @param zero_flag
            @return enc.get_position
            @return enc.get_delta
        ''' 
        
        if(utime.ticks_us() >= self.next_time):
            if zero_flag.read() == self.flag_num:
                self.enc.set_position(0)
                
            self.enc.update() 
            self.next_time += self.commandtime
            self.runs += 1
        return (self.enc.get_position(), self.enc.get_delta())