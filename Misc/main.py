''' @file
    @brief
    @details
    @author
    @author
    @date
'''

import pyb

class Panel:
    def __init__(self,xp,xm,yp,ym,width,length,centerx,centery):
        self.xp_pin = xp
        self.xm_pin = xm
        self.yp_pin = yp
        self.ym_pin = ym
        self.width = width
        self.length = length
        self.centerx = centerx
        self.centery = centery
        
       # self.adcx = pyb.ADC(self.xm_pin)
        #self.adcy = pyb.ADC(self.ym_pin)
        
    def read_x(self):
        self.xp = pyb.Pin(self.xp_pin,pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(self.xm_pin,pyb.Pin.OUT_PP)
        
        self.xp.high()
        self.xm.low()
        
        self.yp = pyb.Pin(self.yp_pin,pyb.Pin.IN)
        #self.ym = pyb.Pin(self.ym_pin,pyb.Pin.IN)
        self.ym = pyb.ADC(self.ym_pin)
        
        self.x_value = self.ym.read()*(self.width/4095) -self.centerx
        
    def read_y(self):
        self.yp = pyb.Pin(self.yp_pin,pyb.Pin.OUT_PP)
        self.ym = pyb.Pin(self.ym_pin,pyb.Pin.OUT_PP)
        
        self.yp.high()
        self.ym.low()
        
        self.xp = pyb.Pin(self.xp_pin,pyb.Pin.IN)
        #self.ym = pyb.Pin(self.ym_pin,pyb.Pin.IN)
        self.xm = pyb.ADC(self.xm_pin)
        
        self.y_value = self.xm.read()*(self.length/4095) -self.centery
        
        
    def read_z(self):
        self.yp = pyb.Pin(self.yp_pin,pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.xp_pin,pyb.Pin.OUT_PP)
        
        self.yp.high()
        self.xp.low()
        
        self.ym = pyb.Pin(self.ym_pin,pyb.Pin.IN)
        #self.ym = pyb.Pin(self.ym_pin,pyb.Pin.IN)
        self.xm = pyb.ADC(self.xm_pin)
        
        self.z_value = self.xm.read()
        
        if 40<self.z_value<4050:
            print(self.x_value, self.y_value)
        
        
if __name__ == '__main__':
    
    test = Panel('PA7','PA1','PA6','PA0',176,100,88,50)
    
    while True:
        test.read_x()
        test.read_y()
        test.read_z()
