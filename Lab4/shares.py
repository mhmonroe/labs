''' @file shares.py
    @brief   Used for sharing data between tasks and main
    @details File created to define classes for Shares and Queues to be used
             for transferring data between files. Currently used for sharing
             data to control motor duty cycle, encoder zeroing, and encoder
             data.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 27, 2021
'''

class Share:
    ''' @brief    Class created to share individual data points.
        @details  Used by writing a piece of data to be shared, which can then
                  be read elsewhere, and is later replaced by writing over with
                  new data.
    '''    
    def __init__(self, start=None):
        ''' @brief Constructor for Share class.
            @param start
        '''    
        self.buffer = start
        
    def write(self,item):
        ''' @brief Method for writing data to a share object.
            @param item
        '''
        self.buffer = item
        
    def read(self):
        ''' @brief Method for reading data stored in a share object.
        '''
        return self.buffer
    
    
class Queue:
    ''' @brief    Class created to share streams of data.
        @details  Used by writing a piece of data to be shared, which can then
                  be read elsewhere, and is later replaced by writing over with
                  new data.
    '''        
    def __init__(self):
        ''' @brief Constructor for Queue class.
        '''            
        self.buffer = []
        
    def put(self, item):
        ''' @brief  Method for storing new data points in the queue by appending
                    the array with the passed data.
            @param  item
        '''
        self.buffer.append(item)
        
    def get(self):
        ''' @brief  Method for reading the first data point in a queue, then
                    removing it.
        '''        
        return self.buffer.pop(0)
    def num_in(self):
        ''' @brief  Method for reading the length of a queue. Can also be used
                     to determine if a queue is empty.
        '''            
        return len(self.buffer)