''' @file    closedloop.py
    @brief   Negative feedback loop to reach a desired input.
    @details Uses a proportional and integral controller to reach a desired value
             using gains and measured data
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 27, 2021
'''
import shares

class ClosedLoop:
    ''' @brief      Class with the logic of the PI controller used for any
                    desired controlling.
        @details    Our code takes the difference between the measured and reference
                    values, then takes a numeric integral of the values. This is 
                    then mulitiplied to an integral gain and a added to the error 
                    value multiplied by the proportional gain.
    '''
    
    def __init__ (self, K_p, K_i):
        ''' @brief      Constructor for the ClosedLoop class
            @param      K_p
            @param      K_i
        '''
        self.Kp = K_p
        self.Ki = K_i
        self.prevdiff = 0
        self.integrator = 0
    
    def run(self, ref, meas, CL_time):
        ''' @brief      Takes the error and applies appropiate gains
            @param      ref
            @param      meas
            @param      CL_time
            @return     self.error
        '''
        self.difference = (ref - meas)
        self.CL_time = CL_time
        self.delta = self.difference - self.prevdiff
        self.prevdiff = self.difference
                    
        self.integrator += self.delta*self.CL_time
        
        self.Kp_gain = self.Kp*self.difference
        self.Ki_gain = self.Ki*self.integrator
    
        self.error = self.Kp_gain = self.Ki_gain
        
        return self.error
    
    def get_Kp(self):
        ''' @brief  Returns the proportional gain.
            @return self.Kp
        '''
        return(self.Kp)
    
    def set_Kp(self, Kp_new):
        ''' @brief Sets a new proportional gain.
        '''
        self.Kp = Kp_new
        
    def get_Ki(self):
        ''' @brief  Returns the integrator gain.
            @return self.Ki
        '''
        return(self.Ki)
    
    def set_Ki(self, Ki_new):
        ''' @brief Sets a new integrator gain.
        '''
        self.Ki = Ki_new

    
    
        
       
        
if __name__ =='__main__':
        # Adjust the following code to write a test program for your motor class. Any
        # code within the if __name__ == '__main__' block will only run when the
        # script is executed as a standalone program. If the script is imported as
        # a module the code block will not run.
        
        
        # Create a motor driver object and two motor objects. You will need to
        # modify the code to facilitate passing in the pins and timer objects needed
        # to run the motors.
        
        
        error_ang_vel = shares.Share(0)
        ref_ang_vel = shares.Share(0)
        meas_ang_vel = shares.Shares(0)
        K_p_prime = shares.Share(0)
        