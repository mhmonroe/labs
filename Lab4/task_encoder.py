''' @file    task_encoder.py
    @brief   Gets encoder values from encoder.py 
    @details Gets encoder values, passes appropiate values to encoder.py, and 
             pecifically controls the zeroing of the encoder position
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import encoder
import utime


class Task_Encoder:
    ''' @brief A class for interacting with the encoder driver
    '''
    def __init__(self, command_time, timerN, pin1, pin2, flag_num):
        ''' @brief Sets variables for the file and calls the hardware 
            @param timerN    
            @param period 
            @param pin1
            @param pin2
        ''' 
        self.runs = 0
        self.commandtime = command_time
        self.pin1 = pin1
        self.pin2 = pin2
        self.flag_num = flag_num
        
        self.enc = encoder.Encoder(timerN,65535,self.pin1,self.pin2)
        
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.commandtime)
        
              
    def run(self, zero_flag):
        ''' @brief Zeros the posititon of the encoder  
            @param zero_flag
            @return enc.get_position
            @return enc.get_delta
        ''' 
        
        if(utime.ticks_ms() >= self.next_time):
           
            if zero_flag.read() == self.flag_num:
                self.enc.set_position(0)
                print('flag was called')
                zero_flag.write(0)
                
            self.enc.update() 
            self.next_time = utime.ticks_add(utime.ticks_ms(), self.commandtime)
            self.runs += 1
            
        return (self.enc.get_position(), self.enc.get_delta())