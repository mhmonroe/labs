''' @file    encoder.py
    @brief   Gets encoder values from hardware
    @details Interacts with the hardware to report the encoder value
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import pyb
import utime
class Encoder:
    
    def __init__(self, timerN, period, pin1, pin2):
        ''' @brief Sets variables for the file and calls the hardware 
            @param timerN    
            @param period 
            @param pin1
            @param pin2
        ''' 
        self.encoderposition = 0
        self.delta = 0
        self.prevtick = 0
        self.newtick = 0
        self.period = period
        self.timerN = timerN
        #self.commandtime = commandtime
        self.enc_time = 0
        self.prev_time = 0
    
        self.timer = pyb.Timer (self.timerN , prescaler = 0 , period = self.period)
        self.t4ch1 = self.timer.channel(1 , pyb.Timer.ENC_AB , pin = pin1)
        self.t4ch2 = self.timer.channel(2 , pyb.Timer.ENC_AB , pin = pin2)
        
    
    def update(self):
        ''' @brief  Reports encoder postition
            @return Encoder position
        ''' 
        #Calculates time since last measurement was taken
        
        self.enc_time = utime.ticks_diff(utime.ticks_us(), self.prev_time)
        self.prev_time = utime.ticks_us()
        
        # Gets the change in ticks
        self.prevtick = self.newtick
        self.newtick = self.timer.counter()
        self.delta = self.newtick - self.prevtick
        
        # Takes overflow calculations properly
        if self.delta >= self.period/2:
            self.delta -= self.period
        elif self.delta <= -self.period/2:
            self.delta += self.period
            
        # ticks to radians
        self.radians = self.delta *(2*3.14159)/4000
        # radians to rad/s
        if self.enc_time > 0:
            self.rad_sec = self.radians/(self.enc_time/1e6)
            
        # Zeros number instead of creating error
        else:
            self.rad_sec = 0.0
            
        self.encoderposition += self.radians
        
    
    def get_position(self):
        ''' @brief  Reports encoder position
            @return Encoder position
        ''' 
        return self.encoderposition
    
    def set_position(self,position): 
        ''' @brief Zeros the encoder value
            @param position
        ''' 
        self.encoderposition = position
        
        
    def get_delta(self):
        ''' @brief  Reports encoder rate of change
            @return Encoder rate of change
        ''' 
            
        return self.rad_sec