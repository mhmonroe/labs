''' @file    task_user_lab0x04.py
    @brief   Interperets user data and reports appriate values  
    @details The user input into the keyboard will determine which values are
             saved and which values are printed. 
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import utime
import pyb
import closedloop
import array

#positon = 0
#delta = 0

class Task_User:
        
    
    def __init__(self, command_period, collection_period):
        ''' @brief Sets variables for the file and calls the hardware 
            @param command_period    
            @param collection_period
        ''' 
        self.serport = pyb.USB_VCP()
        self.command_period = command_period
        self.collection_period = collection_period
        self.command_next_time = 0 + self.command_period
        self.collect_next_time = 0
        self.collect_data_1 = False
        self.collect_data_2 = False
        self.ref1 = 0
        self.ref2 = 0
        self.closed_loop1 = closedloop.ClosedLoop(1,1)
        self.closed_loop2 = closedloop.ClosedLoop(1,1)
        self.CL_time = 0
        
        #setting array sizes and counters
        self.array_num = 30000/int(self.collection_period)
        self.data_time_array = array.array( 'f', [self.array_num]*0)
        self.data_position_array = array.array( 'f', [self.array_num]*0)
        self.data_delta_array = array.array( 'f', [self.array_num]*0)
        
        
        self.print_data = False
        self.i = 0
        self.k = 0
        self.startTime = utime.ticks_ms()
        
              
    def run(self, data_1, data_2, motor1speed, motor2speed, zero_flag, clear_command ):
        ''' @brief   Main method that controls user interface.
            @details Prints the user interface, controls all logic for commands
                     that the user can input, the logic for collecting and
                     printing motor position and speed data collected, user 
                     input motor speeds.
            @param   data_1
            @param   data_2
            @param   motor1speed
            @param   motor2speed
            @param   zero_flag
            @param   clear_command
        ''' 
        #self.command_time = utime.ticks_diff(utime.ticks_ms(), self.currentTime)
        self.currentTime = utime.ticks_diff(utime.ticks_ms(), self.startTime)
        self.data_1 = data_1
        self.data_2 = data_2
        
        
            
        self.CL = utime.ticks_diff(self.currentTime, self.CL_time)
        self.CL_time = self.currentTime
            
            
        L1 = self.closed_loop1.run(self.ref1,self.data_1[1], self.CL/1000)   
        motor1speed.write(L1)
            
              
        L2 = self.closed_loop2.run(self.ref2,self.data_2[1], self.CL/1000) 
        motor2speed.write(L2)
            
        if self.serport.any():
            command = self.serport.read(1)
                    
            if command == b'g':
                self.collect_next_time = self.collection_period
                self.collect_data_1 = True
                print('collecting data encoder 1...')
                self.startTime = utime.ticks_ms()
                self.currentTime = 0
                # clearing array counter
                self.k = 0
                # zeroing the arrays
                self.data_time_array = array.array('f', [0]*300)
                self.data_position_array = array.array('f', [0]*300)
                self.data_delta_array = array.array('f', [0]*300)
                        
            elif command == b'G':
                self.collect_next_time = self.collection_period
                self.collect_data_2 = True
                print('collecting data encoder 2...')
                self.startTime = utime.ticks_ms()
                self.currentTime = 0
                # clearing array counter
                self.k = 0
                # zeroing the arrays
                self.data_time_array = array.array('f', [0]*300)
                self.data_position_array = array.array('f', [0]*300)
                self.data_delta_array = array.array('f', [0]*300)
                        
            elif command == b's' or command == b'S':
                self.collect_data_1 = False
                self.collect_data_2 = False
                print(' Data collection was stopped ')
                self.print_data = True
                                                
            elif command == b'z':
                zero_flag.write(1)
                print('position motor 1:  0 ')
                        
            elif command == b'Z':
                zero_flag.write(2)
                print('position motor 2:  0 ')
                        
            elif command == b'p':
                print("position motor 1: ", self.data_1[0])
                #print(encoder postion)
                        
            elif command == b'P':
                print("position motor 2: ", self.data_2[0])
                        #print(encoder postion)
                        
            elif command == b'd':
                print("delta motor 1: ", self.data_1[1])
                    #print(encoder delta)
                    
            elif command == b'D':
                print("delta motor 2: ", self.data_2[1])
                        #print(encoder delta)
                        
            elif command == b'm':
                print(' enter motor 1 speed :')
                num_st = ''
                    
                while True:
                        
                    if self.serport.any() == True:
                        char_in = self.serport.read(1).decode()
                        self.serport.write(char_in)
                        if char_in.isdigit() ==True:
                            num_st += char_in
                                #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '-':
                            if num_st == '':
                                num_st += char_in
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '.':
                            num_st += char_in
                        elif char_in == '\x7F':
                            if num_st != '':    
                                num_st_new = num_st
                                num_st = num_st_new[:-1]
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '\r' or char_in == '\n':
                            if float(num_st) < -1000000 or float(num_st) > 1000000:
                                num_st = ''
                                print('Duty cycle must be between -100 and 100. Please press m to enter a new value.')
                                break
                            else:
                                self.ref1 = float(num_st)
                                self.CL_time = self.currentTime    
                                    #motor1speed.write(self.mot_speed1)
                                print("speed of motor 1: ", self.ref1, 'rad/s')
                                break
                        
            elif command == b'M':
                print(' enter motor 2 speed :')
                num_st = ''
                    
                while True:
                        
                    if self.serport.any() == True:
                        char_in = self.serport.read(1).decode()
                        self.serport.write(char_in)
                        if char_in.isdigit() ==True:
                            num_st += char_in
                                #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '-':
                            if num_st == '':
                                num_st += char_in
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '.':
                            num_st += char_in
                        elif char_in == '\x7F':
                            if num_st != '':    
                                num_st_new = num_st
                                num_st = num_st_new[:-1]
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '\r' or char_in == '\n':
                            if float(num_st) < -100000 or float(num_st) > 100000:
                                num_st = ''
                                print('Duty cycle must be between -100 and 100. Please press M to enter a new value.')
                                break
                            else:
                                self.ref2 = float(num_st)
                                self.CL_time = self.currentTime
                                    #motor2speed.write(self.mot_speed2)
                                print("speed of motor 2: ", self.ref2, 'rad/s')
                                break
            elif command == b'k':
                print(' enter a proportional gain for motor 1:')
                num_st = ''
                    
                while True:
                        
                    if self.serport.any() == True:
                        char_in = self.serport.read(1).decode()
                        self.serport.write(char_in)
                        if char_in.isdigit() ==True:
                            num_st += char_in
                                #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '-':
                            if num_st == '':
                                num_st += char_in
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '.':
                            num_st += char_in
                        elif char_in == '\x7F':
                            if num_st != '':    
                                num_st_new = num_st
                                num_st = num_st_new[:-1]
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '\r' or char_in == '\n':
                            if float(num_st) < -100000 or float(num_st) > 100000:
                                num_st = ''
                                print('Duty cycle must be between -100 and 100. Please press m to enter a new value.')
                                break
                            else:
                                self.closed_loop1.set_Kp(float(num_st))
                                    
                                    #motor1speed.write(self.mot_speed1)
                                print("proportional gain for motor 1: ", num_st, '%s/rad')
                                break
                        
            elif command == b'K':
                print(' enter a proportional gain for motor 2:')
                num_st = ''
                    
                while True:
                        
                    if self.serport.any() == True:
                        char_in = self.serport.read(1).decode()
                        self.serport.write(char_in)
                        if char_in.isdigit() ==True:
                            num_st += char_in
                                #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '-':
                            if num_st == '':
                                num_st += char_in
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '\x7F':
                            if num_st != '':    
                                num_st_new = num_st
                                num_st = num_st_new[:-1]
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '.':
                            num_st += char_in
                        elif char_in == '\r' or char_in == '\n':
                            if float(num_st) < -10000 or float(num_st) > 100000:
                                num_st = ''
                                print('Duty cycle must be between -100 and 100. Please press m to enter a new value.')
                                break
                            else:
                                self.closed_loop2.set_Kp(float(num_st))
                                    
                                    #motor1speed.write(self.mot_speed1)
                                print("proportional gain for motor 2: ", num_st, '%s/rad')
                                break
                        
            elif command == b'i':
                print(' enter a integrator gain for motor 1:')
                num_st = ''
                    
                while True:
                        
                    if self.serport.any() == True:
                        char_in = self.serport.read(1).decode()
                        self.serport.write(char_in)
                        if char_in.isdigit() ==True:
                            num_st += char_in
                                #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '-':
                            if num_st == '':
                                num_st += char_in
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '.':
                            num_st += char_in
                        elif char_in == '\x7F':
                            if num_st != '':    
                                num_st_new = num_st
                                num_st = num_st_new[:-1]
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '\r' or char_in == '\n':
                            if float(num_st) < -10000 or float(num_st) > 100000:
                                num_st = ''
                                print('Duty cycle must be between -100 and 100. Please press m to enter a new value.')
                                break
                            else:
                                self.closed_loop1.set_Ki(float(num_st))
                                    
                                    #motor1speed.write(self.mot_speed1)
                                print("proportional gain for motor 1: ", num_st, '%s/rad')
                                break
                        
            elif command == b'I':
                print(' enter a integrator gain for motor 2:')
                num_st = ''
                    
                while True:
                        
                    if self.serport.any() == True:
                        char_in = self.serport.read(1).decode()
                        self.serport.write(char_in)
                        if char_in.isdigit() ==True:
                            num_st += char_in
                                #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '-':
                            if num_st == '':
                                num_st += char_in
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '\x7F':
                            if num_st != '':    
                                num_st_new = num_st
                                num_st = num_st_new[:-1]
                                    #print(' enter motor 1 duty cycle :', num_st, end='\r')
                        elif char_in == '.':
                            num_st += char_in
                        elif char_in == '\r' or char_in == '\n':
                            if float(num_st) < -100000 or float(num_st) > 100000:
                                num_st = ''
                                print('Duty cycle must be between -100 and 100. Please press m to enter a new value.')
                                break
                            else:
                                self.closed_loop2.set_Ki(float(num_st))
                                    
                                    #motor1speed.write(self.mot_speed1)
                                print("integrator gain for motor 2: ", num_st, '%s/rad')
                                break
                                
            elif command == b'c' or command == b'C':
                clear_command.write(True)
                print('cleared fault')
                        #print(encoder postion)
                        
        if self.collect_data_1 and self.currentTime >= self.collect_next_time:
            
            self.array_time = float(self.currentTime)/100
            self.data_time_array[self.k] = round(self.array_time,1)/10
            self.data_position_array[self.k] = (data_1[0])
            self.data_delta_array[self.k] = (data_1[1])
            self.collect_next_time += self.collection_period
            
            #counter to determine array list lengths
            self.k +=1
            if self.currentTime >= 30000:
                self.print_data = True
                self.collect_data_1 = False
                    
        if self.collect_data_2 and self.currentTime >= self.collect_next_time:
            
            self.array_time = float(self.currentTime)/100
            self.data_time_array[self.k] = round(self.array_time,1)/10
            self.data_position_array[self.k] = (data_2[0])
            self.data_delta_array[self.k] = (data_2[1])
            self.collect_next_time += self.collection_period
            
            #counter to determine array list lengths
            self.k +=1
            if self.currentTime >= 30000:
                self.print_data = True
                self.collect_data_2 = False
            
                    
        if self.print_data ==True and self.i < 300 :
            if self.data_time_array[self.i] > 0:
                print(self.data_time_array[self.i],'s', self.data_position_array[self.i],'rad', self.data_delta_array[self.i],'rad/s')
                self.i += 1
            else:
                pass
        else:
            self.print_data = False
            self.i = 0
         
      
        
    def instructions(self):
        ''' @brief Method to print out the cool instructions  
        ''' 
        print('+-----------------------------------------+')
        print('| z : Zero the position of the encoder 1  |')
        print('| Z : Zero the position of the encoder 2  |')
        print('| p : Print out the position of encoder 1 |')
        print('| P : Print out the position of encoder 2 |')
        print('| d : Print out the delta for encoder 1   |')
        print('| D : Print out the delta for encoder 2   |')
        print('| m : Enter angular velocity for motor 1  |')
        print('| M : Enter angular velocity for motor 2  |')
        print('| c : Clear fault condition on driver     |')
        print('| g : Collect encoder 1 data for 30 secs  |')
        print('|        and print it to PuTTY as a list  |')
        print('| G : Collect encoder 2 data for 30 secs  |')
        print('|        and print it to PuTTY as a list  |')
        print('| s : End data collection prematurely     |')
        print('+-----------------------------------------+')  
        
        
   
        
    