''' @file    task_driver.py
    @brief   Task used for controlling motors.
    @details The task gets passed information that is then used with driver.py
             to control the speed of the motors.
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 21, 2021
'''

import driver
import pyb


class Task_Driver:
    ''' @brief    Class created to interface with the motor driver.
        @details  Contains the methods used to allow other tasks to control
                  motors without needing its own motor logic.
    '''     
    def __init__(self):
        ''' @brief   Constructor for Task driver class.
            @details Instantiates objects of the motor class to be controlled
                     with other methods.
        '''            
        self.motor_drv     = driver.DRV8847( pyb.Pin.cpu.A15, pyb.Pin.OUT_PP, pyb.Pin.cpu.B2 )     #include fault info at some point
        self.motor_1       = self.motor_drv.motor( pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
        self.motor_2       = self.motor_drv.motor( pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)
            
        # Enable the motor driver
        self.motor_drv.enable()
            
        # Set the duty cycle of the first motor to 40 percent and the duty cycle of
        # the second motor to 60 percent
        
    def run(self, motor1speed, motor2speed, clear_command):    
        ''' @brief Method called by other tasks to control speed of motors.
            @param motor1speed
            @param motor2speed
            @param clear_command
        '''        
        self.motor_1.set_duty(motor1speed.read())
        self.motor_2.set_duty(motor2speed.read())
        
        if clear_command.read():
            self.motor_drv.enable()
            clear_command.write(False)