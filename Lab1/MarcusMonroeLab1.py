#''' @breif  Finite State Machine Skeleton Code
#    @details
#    @author Marcus Monroe 
#'''



import pyb      # Imports Nucleo Functions
import math     
import time

## Sets up interaction with the button
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

## Sets up interaction with LED
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)


button = False

def onButtonPressFCN(IRQ_src):
    global button
    button = True
 
## Sawtooth wave function   
def UPDATE_STW(current_time):
    return 100*(current_time%1.0)

## Square wave function 
def UPDATE_SQW(current_time):
    return 100*(current_time%1.0 < 0.5)

## Sine wave function
def UPDATE_SIW(current_time):
    return 100*(math.sin(current_time))

## Counts time 
def reset_timer():
    startTime = time.ticks_ms()
    
def update_timer():
    stopTime = time.ticks_ms()
    duration = time.ticks_diff(stopTime, startTime)
    

    
if __name__=='__main__':
    ## Transitions to state 1
    state = 1
    print('Welcome to the LED light show')
    print(' Press control c to get out of this code at any time')
    print(' Press the blue button to begin')
    
    #
    counter = 0
    
    # Loads data about the button 
    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=onButtonPressFCN)
    
   
   
    
    while True:     
        try:
            # 
            if state == 1:
                
                if button == True:
                    state = 2
                    print(' LED will display square wave')
                    counter += 1
                    button = False
             
            # Runs the Square Wave
            elif state == 2:
               
                current_time = time.ticks_ms()
                bright = UPDATE_SQW(current_time)
                t2ch1.pulse_width_percent(bright)
                
                if button == True:
                    state = 3
                    print(' LED will display sine wave')
                    counter += 1
                    button = False
              
            # Runs Sawtooth Wave
            elif state == 3 :
                
                current_time = time.ticks_ms()
                bright = UPDATE_STW(current_time)
                t2ch1.pulse_width_percent(bright)
                
                if button == True:
                    state = 4 
                    print(' LED will display sawtooth wave')
                    counter += 1
                    button = False
             
            # Runs Sine wave
            elif state == 4:
               
                current_time = time.ticks_ms()
                bright = UPDATE_SIW(current_time)
                t2ch1.pulse_width_percent(bright) 
               
                if button == True:
                    state = 2
                    print(' LED will display square wave')
                    counter += 1
                    button = False
                    
        # Breaks the code when commanded
        except:
            print(' Goodbye ')
            break
              