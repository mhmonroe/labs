## @file 305_Lab0x01_Page.py
#  @brief Mainpage for the ME305 Lab 0x01
# 
## @page 305_lab0x01 305 Lab 0x01: LED Project
#
#  Sourcecode here \n
#  <a class="custom" href="https://bitbucket.org/mhmonroe/labs/src/master/Lab0x01.py" 
#  target="_blank" rel="noopener noreferrer">Lab0x01 Source Code</a> \n\n
#  
#  @image      html cow.jpg " " width = 80%
#  @details    \n <CENTER> **Figure 1.** State Transition Diagram of LED Lab. </CENTER> \n
#
#  My Lab Partner is Marco Leen. \n\n
# \htmlonly <div style="text-align:center;">
# <iframe width="560" height="315" src="https://www.youtube.com/embed/WrQGxU4is74" 
# title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; 
# clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
# </div>\endhtmlonly
# 
#  \ref Lab0x01.py