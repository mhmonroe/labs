''' @file Lab0x01.py
    @breif   LED displays based on user input
    @details This code turns 
    @author  Marcus Monroe
    @author  Marco Leen
    @date    October 8, 2021
'''



import pyb      # Imports Nucleo Functions
import math    
import utime



def onButtonPressFCN(IRQ_src):
    ''' @breif     Reports button press to rest of code
        @param IRQ_src button report value  
        @param button 
        @return    Button value
    '''
    global button
    button = True
 

def UPDATE_STW(current_time):
    ''' @breif      Runs sawtooth wave
        @param current_time  
        @return     Sawtooth wave for for light
    '''
    return (current_time%1000)/10


def UPDATE_SQW(current_time):
    ''' @breif      Runs square wave
        @param current_time  
        @return     Square wave for for light
    '''
    return 100*(current_time%1000 < 500)


def UPDATE_SIW(current_time):
    ''' @breif      Runs sine wave
        @param current_time  
        @return     Sine wave for for light
    '''
    return 100*(0.5+0.5*math.sin(2*math.pi*(1/10000)*current_time))

## Counts time
def reset_timer():
    ''' @breif      Zeros the timer when button is pressed
        @return     Start time of LED display
    '''
    global startTime
    startTime = utime.ticks_ms()
   
def update_timer():
    ''' @breif      Records the duration of the LED display
        @param current_time
        @return     Current time 
    '''
    stopTime = utime.ticks_ms()
    global current_time
    current_time = utime.ticks_diff(stopTime, startTime)
   

   
if __name__=='__main__':
    ## Transitions to state 1
    state = 1
    print('Welcome to the LED light show.')
    print('Press Ctrl+C to get out of this code at any time.')
    print('Press the blue button, B1, to begin cycling through LED patterns.')
   
    ## Sets up interaction with the button
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
   
    ## Sets up interaction with LED
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
   
   
    button = False
    #
    counter = 0
   
    # Loads data about the button
    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=onButtonPressFCN)
   
   
   
   
    while True:    
        try:
            #
            if state == 1:
               
                if button == True:
                    state = 2
                    print('LED will display square wave.')
                    counter += 1
                    reset_timer()
                    button = False
             
            # Runs the Square Wave
            elif state == 2:
               
                update_timer()
               
                t2ch1.pulse_width_percent(UPDATE_SQW(current_time))
               
                if button == True:
                   
                    reset_timer()
                    state = 3
                    print('LED will display sawtooth wave.')
                    counter += 1
                    button = False
             
            # Runs Sawtooth Wave
            elif state == 3 :
               
                update_timer()
               
                t2ch1.pulse_width_percent(UPDATE_STW(current_time))
               
                if button == True:
                   
                    reset_timer()
                    state = 4
                    print('LED will display sine wave.')
                    counter += 1
                    button = False
             
            # Runs Sine wave
            elif state == 4:
               
                update_timer()
               
                t2ch1.pulse_width_percent(UPDATE_SIW(current_time))
               
                if button == True:
                   
                    reset_timer()
                    state = 2
                    print('LED will display square wave.')
                    counter += 1
                    #print(counter)
                    button = False
                   
        # Breaks the code when commanded
        except KeyboardInterrupt:
            print(' Goodbye ')
            break